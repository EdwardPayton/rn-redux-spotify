import React from 'react';
import { Text } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import { StackNavigator } from 'react-navigation';
import configureStore from './src/store';
import Login from './src/screens/Login';
import Home from './src/screens/Home';
import Loading from './src/components/Loading';

const { store, persistor } = configureStore();

// persistor.purge(); // TODO - if persist data exists dont call the apis again (creates dupes)

const RootNav = StackNavigator({
  Login: { screen: Login },
  Home: { screen: Home },
}, {
  headerMode: 'none',
});

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={<Loading />} persistor={persistor}>
        <RootNav />
      </PersistGate>
    </Provider>
  )
};

export default App;
