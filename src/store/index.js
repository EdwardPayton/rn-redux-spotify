import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import logger from 'redux-logger';
import storage from 'redux-persist/lib/storage';
import rootReducer from '../reducers';
import authMiddleware from '../middleware/authMiddleware';

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['spotifyPlayer'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export default () => {
  const store = createStore(persistedReducer, applyMiddleware(authMiddleware, thunk, logger));
  // let store = createStore(persistedReducer, applyMiddleware(authMiddleware, thunk))
  const persistor = persistStore(store);
  return { store, persistor };
};
