// @flow
import * as React from 'react';
import {
  View, Text, FlatList, TouchableOpacity, Keyboard,
} from 'react-native';
import { connect } from 'react-redux';
import Spotify from 'rn-spotify-sdk';
import MaterialIcon from '../components/MaterialIcon';
import Image from '../components/Image';
import {
  setPlayingState, playNext, playPrev, setCurrentTrack,
} from '../actions';
import { default as s } from '../styles';
import { default as t } from '../styles/theme';

type Props = {
  player: {
    playPlaying: boolean,
    currentPlaylist: {
      images: any[],
      tracks: { items: any[] }
    }
  },
  setPlayingState: boolean => mixed,
  playNext: () => mixed,
  playPrev: () => mixed
};

type State = {
  pagerIndex: null | number,
  keyboardOpen: boolean
};

class PlayerBar extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      pagerIndex: null,
      keyboardOpen: false,
    };

    (this: any).setNewPagerIndex = this.setNewPagerIndex.bind(this);
    (this: any).keyboardIsOpen = this.keyboardIsOpen.bind(this);
    (this: any).keyboardIsClosed = this.keyboardIsClosed.bind(this);
  }

  setPlayState(): void { // eslint-disable-line react/sort-comp
    const {
      player: { playPlaying },
    } = this.props;

    playPlaying
      ? this.props.setPlayingState(false)
      : this.props.setPlayingState(true);
  }

  setNewPagerIndex(e): void {
    const offset = e.nativeEvent.contentOffset.x;
    const viewSize = e.nativeEvent.layoutMeasurement;
    const pagerIndex = Math.floor(offset / viewSize.width);

    if (pagerIndex === this.state.pagerIndex) return;

    if (this.state.pagerIndex !== null && pagerIndex > this.state.pagerIndex) this.props.playNext();
    else this.props.playPrev();

    this.setState({ pagerIndex });
  }

  keyboardIsOpen(): void { // eslint-disable-line react/sort-comp
    this.setState({ keyboardOpen: true });
  }

  keyboardIsClosed(): void {
    this.setState({ keyboardOpen: false });
  }

  componentDidMount(): void {
    (this: any)._metadataListener = Spotify.addListener('metadataChange', (data) => {
      const { currentTrack: { indexInContext } } = data.metadata;
      this.setState({ pagerIndex: indexInContext }); // TODO this isnt working after changing playlist OR clicking play from start OR after pausing
    });

    (this: any)._keyboardOpenListener = Keyboard.addListener('keyboardDidShow', this.keyboardIsOpen);
    (this: any)._keyboardClosedListener = Keyboard.addListener('keyboardDidHide', this.keyboardIsClosed);
  }

  // componentDidUpdate(prevProps) {
  //   const { currentPlaylist } = this.props.player;
  //   const currentUri = currentPlaylist && currentPlaylist.uri;
  //   const prevPlaylist = prevProps.player.currentPlaylist;
  //   const prevUri = prevPlaylist && prevPlaylist.uri;

  //   if (currentUri && prevUri && (currentUri !== prevUri)) {
  //     console.warn('New playlist', currentPlaylist, currentUri, prevUri);
  //   }

  //   // const allTracks = currentPlaylist && currentPlaylist.tracks.items;
  //   // const trackNum = allTracks.map(track => track.track.uri).indexOf(uri);

  //   // this.flatListRef.scrollToIndex({animated: true, index: trackNum})
  // }

  componentWillUnmount(): void {
    (this: any)._metadataListener.remove();
    (this: any)._keyboardOpenListener.remove();
    (this: any)._keyboardClosedListener.remove();
  }

  render(): React.Node {
    const {
      player: { playPlaying, currentPlaylist },
    } = this.props;
    const barHeight = this.state.keyboardOpen ? 0 : t.barHeight;

    return (
      <View style={[s.playBar, { height: barHeight }]}>
        {currentPlaylist && currentPlaylist.images.length
          ? (
            <Image
              uri={currentPlaylist.images[0].url}
              width={t.barHeight}
              height={t.barHeight}
              borderRadius={0}
            />
          )
          : <View style={s.playBarEdge} />
        }

        {currentPlaylist && this.state.pagerIndex !== null && this.state.pagerIndex >= 0
          ? (
            <FlatList
              horizontal
              pagingEnabled
              showsHorizontalScrollIndicator={false}
              onMomentumScrollEnd={this.setNewPagerIndex}
              initialScrollIndex={this.state.pagerIndex}
              ref={(ref) => { (this: any).flatListRef = ref; }}
              data={currentPlaylist.tracks.items}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => (
                <View style={s.playBarCenter}>
                  <Text numberOfLines={1} style={s.colorText}>
                    {item.track.name}
                  </Text>
                  <Text numberOfLines={1} style={s.colorBgBright}>
                    {item.track.artists[0].name}
                  </Text>
                </View>
              )}
            />
          )
          : (
            <View style={s.playBarCenter} />
          )
}

        <View style={s.playBarEdge}>
          <TouchableOpacity
            style={s.controlPlay}
            onPress={() => this.setPlayState()}
          >
            {playPlaying
              ? <MaterialIcon name="pause" size={50} color={t.fgLight} />
              : <MaterialIcon name="play-arrow" size={50} color={t.fgLight} />
            }
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    player: state.spotifyPlayer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setPlayingState: state => dispatch(setPlayingState(state)),
    playNext: () => dispatch(playNext()),
    playPrev: () => dispatch(playPrev()),
    setCurrentTrack: track => dispatch(setCurrentTrack(track)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerBar);
