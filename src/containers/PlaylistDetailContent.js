// @flow
import * as React from 'react';
import { FlatList } from 'react-native';
import { connect } from 'react-redux';
import { startPlayback } from '../actions';
import PlaylistRow from '../components/PlaylistRow';

type Props = {
  api: {
    playlistTracks: { tracks: { items: {} } }
  },
  player: {
    playActive: boolean
  },
  startPlayback: (playlistTracks: {}, index: number, seconds: number) => mixed,
};

class PlaylistDetailContent extends React.Component<Props> {
  play(index) {
    const {
      api: { playlistTracks },
    } = this.props;
    this.props.startPlayback(playlistTracks, index, 0);
  }

  render(): React.Node {
    const {
      api: { playlistTracks },
      player: { playActive },
    } = this.props;

    return (
      <FlatList
        style={{ marginBottom: playActive ? 80 : 20 }}
        data={playlistTracks.tracks.items}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <PlaylistRow
            title={item.track.name}
            subTitle={item.track.artists[0].name}
            image={item.track.album.images[0]}
            action={() => this.play(index)}
          />
        )}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    api: state.spotifyApi,
    player: state.spotifyPlayer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    startPlayback: (playlist, index, seconds) => dispatch(startPlayback(playlist, index, seconds)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PlaylistDetailContent);
