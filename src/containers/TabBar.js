// @flow
import * as React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import MaterialIcon from '../components/MaterialIcon';
import { default as s } from '../styles';
import { default as t } from '../styles/theme';

type Props = {
  navigation: any, // TODO
  jumpToIndex: any // TODO
}
const TabBar = (props: Props): React.Node => {
  const {
    state: { routes, index },
  } = props.navigation;

  const iconMap = {
    Playlists: 'queue-music',
    Create: 'add',
    Info: 'person',
  };

  return (
    <View style={s.tabBar}>
      {routes && routes.map((route, i) => {
        const color = i === index ? t.fgPale : t.bgText;
        return (
          <TouchableOpacity
            style={s.tabTab}
            key={route.key}
            onPress={() => props.jumpToIndex(i)}
          >
            <Text style={[s.tabText, { color }]}>
              {route.key}
            </Text>
            <View style={s.tabIconWrapper}>
              <MaterialIcon name={iconMap[route.key]} size={35} color={color} />
            </View>
          </TouchableOpacity>
        );
      })
      }
    </View>
  );
};

export default TabBar;
