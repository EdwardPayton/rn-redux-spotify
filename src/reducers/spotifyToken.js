import {
  RENEW_TOKEN_PENDING, RENEW_TOKEN_SUCCESS, RENEW_TOKEN_FAILURE,
} from '../constants';

const initialState = {
  tokenPending: false,
  tokenSuccess: false,
  tokenError: false,
  tokenObj: null,
  expires: 0, // Temp fix - tokenObj date is not correct - https://github.com/lufinkey/react-native-spotify/issues/58
};

export default function spotifyToken(state = initialState, action) {
  switch (action.type) {
    case RENEW_TOKEN_PENDING:
      return {
        ...state,
        tokenPending: true,
        tokenSuccess: false,
        tokenError: false,
        tokenObj: null,
      };
    case RENEW_TOKEN_SUCCESS:
      return {
        ...state,
        tokenPending: false,
        tokenSuccess: true,
        tokenObj: action.data,
        expires: action.expires,
      };
    case RENEW_TOKEN_FAILURE:
      return {
        ...state,
        tokenPending: false,
        tokenError: true,
        expires: 0,
      };
    default:
      return state;
  }
}
