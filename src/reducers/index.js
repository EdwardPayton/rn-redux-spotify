import { combineReducers } from 'redux';
import spotifyInit from './spotifyInit';
import spotifyAuth from './spotifyAuth';
import spotifyToken from './spotifyToken';
import spotifyUser from './spotifyUser';
import spotifyApi from './spotifyApi';
import spotifyPlayer from './spotifyPlayer';

const rootReducer = combineReducers({
  spotifyInit,
  spotifyAuth,
  spotifyToken,
  spotifyUser,
  spotifyApi,
  spotifyPlayer,
});

export default rootReducer;
