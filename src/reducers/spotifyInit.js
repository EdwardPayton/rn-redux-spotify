import { INIT_PENDING, INIT_SUCCESS, INIT_FAILURE } from '../constants';

const initialState = {
  initPending: false,
  initSuccess: false,
  initError: false,
};

export default function spotifyInit(state = initialState, action) {
  switch (action.type) {
    case INIT_PENDING:
      return {
        ...state,
        initPending: true,
        initSuccess: false,
        initError: false,
      };
    case INIT_SUCCESS:
      return {
        ...state,
        initPending: false,
        initSuccess: true,
      };
    case INIT_FAILURE:
      return {
        ...state,
        initPending: false,
        initError: true,
      };
    default:
      return state;
  }
}
