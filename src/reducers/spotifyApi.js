import {
  ACESSS_API_PENDING,
  GET_USER_PLAYLISTS_SUCCESS,
  GET_PLAYLISTS_FAILURE,
  PLAYLIST_TRACKS_SUCCESS,
  PLAYLIST_TRACK_FAILURE,
  GET_TOP_ARTISTS_SUCCESS,
  GET_TOP_ARTISTS_FAILURE,
  GET_RECOMENDATIONS_SUCCESS,
  GET_RECOMENDATIONS_FAILURE,
  CREATE_PLAYLIST_SUCCESS,
  CREATE_PLAYLIST_FAILURE,
  TRACKS_ADDED_SUCCESS,
  TRACKS_ADDED_FAILURE,
} from '../constants';

const initialState = {
  apiPending: false,
  playlistSuccess: false, // TODO remove
  playlists: [],
  playlistError: false,
  playlistTracks: undefined,
  playlistTracksError: false,
  topArtists: undefined, // {}
  topArtistsError: false,
  recomendations: undefined, // {}
  recomendationsError: false,
  newPlaylist: undefined, // {}
  newPlaylistError: false,
  tracksAdded: false,
  tracksAddedError: false,
};

export default function spotifyApi(state = initialState, action) {
  switch (action.type) {
    case ACESSS_API_PENDING:
      return {
        ...state,
        apiPending: true,
        playlistSuccess: false,
        playlistError: false,
        playlistTracksError: false,
        topArtistsError: false,
        recomendationsError: false,
        tracksAdded: false,
        tracksAddedError: false,
      };
    case GET_USER_PLAYLISTS_SUCCESS:
      return {
        ...state,
        apiPending: false,
        playlistSuccess: true,
        playlists: action.data,
      };
    case GET_PLAYLISTS_FAILURE:
      return {
        ...state,
        apiPending: false,
        playlistError: true,
      };
    case PLAYLIST_TRACKS_SUCCESS:
      return {
        ...state,
        apiPending: false,
        playlistTracks: action.data,
      };
    case PLAYLIST_TRACK_FAILURE:
      return {
        ...state,
        apiPending: false,
        playlistTracksError: true,
      };
    case GET_TOP_ARTISTS_SUCCESS:
      return {
        ...state,
        apiPending: false,
        topArtists: action.data,
      };
    case GET_TOP_ARTISTS_FAILURE:
      return {
        ...state,
        apiPending: false,
        topArtistsError: undefined,
      };
    case GET_RECOMENDATIONS_SUCCESS:
      return {
        ...state,
        apiPending: false,
        recomendations: action.data,
      };
    case GET_RECOMENDATIONS_FAILURE:
      return {
        ...state,
        apiPending: false,
        recomendationsError: true,
      };
    case CREATE_PLAYLIST_SUCCESS:
      return {
        ...state,
        apiPending: false,
        newPlaylist: action.data,
      };
    case CREATE_PLAYLIST_FAILURE:
      return {
        ...state,
        apiPending: false,
        newPlaylistError: true,
      };
    case TRACKS_ADDED_SUCCESS:
      return {
        ...state,
        tracksAdded: true,
      };
    case TRACKS_ADDED_FAILURE:
      return {
        ...state,
        tracksAddedError: true,
      };
    default:
      return state;
  }
}
