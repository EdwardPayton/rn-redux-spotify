import { GET_USER_PENDING, GET_USER_SUCCESS, GET_USER_FAILURE } from '../constants';

const initialState = {
  getUserPending: false,
  getUserSuccess: false,
  getUserError: false,
  userErrorMsg: null,
  userObj: null,
};

export default function spotifyUser(state = initialState, action) {
  switch (action.type) {
    case GET_USER_PENDING:
      return {
        ...state,
        getUserPending: true,
        getUserSuccess: false,
        getUserError: false,
        userErrorMsg: null,
        userObj: null,
      };
    case GET_USER_SUCCESS:
      return {
        ...state,
        getUserPending: false,
        getUserSuccess: true,
        userObj: action.data,
      };
    case GET_USER_FAILURE:
      return {
        ...state,
        getUserPending: false,
        getUserError: true,
        userErrorMsg: action.data,
      };
    default:
      return state;
  }
}
