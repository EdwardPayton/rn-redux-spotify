import {
  PLAY_PENDING,
  PLAY_SUCCESS,
  PLAY_PAUSE,
  PLAY_RESUME,
  PLAY_FAILURE,
  TRACK_CHANGE_SUCCESS,
  TRACK_CHANGE_FAILURE,
  SET_CURRENT_TRACK,
} from '../constants';

const initialState = {
  playerPending: false,
  playActive: false,
  playPlaying: false,
  playError: false,
  playErrorMsg: null,
  currentPlaylist: null,
  track: null,
};

export default function spotifyPlayer(state = initialState, action) {
  switch (action.type) {
    case PLAY_PENDING:
      return {
        ...state,
        playerPending: true,
        playActive: true,
        playError: false,
        playErrorMsg: null,
        track: null,
      };
    case PLAY_SUCCESS:
      return {
        ...state,
        playerPending: false,
        playPlaying: true,
        currentPlaylist: action.data && action.data,
      };
    case PLAY_PAUSE:
      return {
        ...state,
        playerPending: false,
        playPlaying: false,
      };
    case PLAY_RESUME:
      return {
        ...state,
        playerPending: false,
        playPlaying: true,
      };
    case PLAY_FAILURE:
      return {
        ...state,
        playerPending: false,
        playError: true,
        playErrorMsg: action.data,
      };
    case TRACK_CHANGE_SUCCESS:
      return {
        ...state,
        playerPending: false,
        playPlaying: true,
      };
    case TRACK_CHANGE_FAILURE:
      return {
        ...state,
        playerPending: false,
        playError: true,
      };
    case SET_CURRENT_TRACK:
      return {
        ...state,
        track: action.data,
      };
    default:
      return state;
  }
}
