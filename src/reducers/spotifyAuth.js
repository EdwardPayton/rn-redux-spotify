import {
  STATUS_PENDING, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT_SUCCESS,
} from '../constants';

const initialState = {
  loginStatusPending: false,
  userLoggedIn: false,
  loginError: false,
};

export default function spotifyAuth(state = initialState, action) {
  switch (action.type) {
    case STATUS_PENDING:
      return {
        ...state,
        loginStatusPending: true,
        userLoggedIn: action.currentStatus,
        loginError: false,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        loginStatusPending: false,
        userLoggedIn: true,
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        loginPending: false,
        loginError: true,
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        loginStatusPending: false,
        userLoggedIn: false,
      };
    // case GET_AUTH_PENDING:
    //   return {
    //     ...state,
    //     authObj: null,
    //     authError: false
    //   }
    // case GET_AUTH_SUCCESS:
    //   return {
    //     ...state,
    //     authObj: action.data
    //   }
    // case GET_AUTH_FAILURE:
    //   return {
    //     ...state,
    //     authObj: null,
    //     authError: true
    //   }
    default:
      return state;
  }
}
