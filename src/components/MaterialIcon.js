// @flow
import * as React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { default as t } from '../styles/theme';

type Props = {
  name: string,
  size?: number,
  color?: string,
};

const MaterialIcon = (props: Props): React.Node => {
  const { name, size, color } = props;
  return <Icon name={name} size={size} color={color} />;
};

MaterialIcon.defaultProps = {
  size: t.iconSizeM,
  color: t.fgPale,
};

export default MaterialIcon;
