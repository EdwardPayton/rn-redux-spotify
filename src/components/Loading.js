
import * as React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import { default as s } from '../styles';

const Loading = (): React.Node => (
  <View style={s.loadWrapper}>
    <ActivityIndicator animating style={s.loadIndicator} />
    <Text style={s.loadMessage}>
Loading...
    </Text>
  </View>
);

export default Loading;
