// @flow
import * as React from 'react';
import { View, Text } from 'react-native';
import Button from './Button';
import { default as s } from '../styles';
import { default as t } from '../styles/theme';
import Image from './Image';

type Props = {
  title: string,
  subTitle?: string,
  image?: {
    url: string
  },
  action: () => mixed
};
const CreateRow = (props: Props): React.Node => {
  const {
    title, subTitle, image, action,
  } = props;
  return (
    <View style={s.createRowWrapper}>
      <View style={s.createRowColumn}>
        {image && <Image uri={image.url} width={180} height={180} borderRadius={6} />}
      </View>

      <View style={[s.createRowColumn, s.createRowColumnR]}>
        <Text style={[s.textLarge, s.colorWhite]}>
          Based on:
        </Text>
        <Text style={[s.textLarge, s.colorWhite]}>
          {title}
        </Text>
        {subTitle && (
          <Text style={s.colorLight}>
            {subTitle}
          </Text>
        )}
        <View style={s.createButtonWrapper}>
          <Button
            title="See the tracks"
            onPress={action}
            bgColor={t.bgBright}
            borderFrom={t.bgBrightPale}
            align="left"
          />
        </View>
      </View>
    </View>
  );
};

CreateRow.defaultProps = {
  subTitle: undefined,
  image: undefined,
};

export default CreateRow;
