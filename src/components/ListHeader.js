// @flow
import * as React from 'react';
import { View, Text } from 'react-native';
import ButtonIcon from './ButtonIcon';
import { default as s } from '../styles';
import { default as t } from '../styles/theme';

type Props = {
  title: string,
  backAction?: () => mixed,
  lines?: number,
  fontSize?: number
}

const ListHeader = (props: Props): React.Node => {
  const {
    title,
    backAction,
    lines,
    fontSize,
  } = props;
  return (
    <View style={s.headerBar}>
      {backAction
        && <ButtonIcon icon="arrow-back" action={backAction} />
      }
      <Text style={[s.headerTitle, { fontSize }]} numberOfLines={lines}>
        {title}
      </Text>
    </View>
  );
};

ListHeader.defaultProps = {
  backAction: undefined,
  lines: 1,
  fontSize: t.fontSizeL,
};

export default ListHeader;
