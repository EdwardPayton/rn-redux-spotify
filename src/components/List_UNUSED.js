import React, { Component } from 'react';
import { FlatList } from 'react-native';

export default class componentName extends Component {
  render() {
    // style onRefresh refreshing data listHeaderComponent renderItem (whole function)


    return (
      <FlatList
        // onScroll={aa}
        style={{ marginBottom: playActive ? 80 : 20 }}
        onRefresh={this.onRefresh}
        refreshing={apiPending}
        data={playlists}
        keyExtractor={(item, index) => index.toString()}
        ListHeaderComponent={
          <ListHeader title="Playlists" fontSize={t.fontSizeTitle} />
        }
        renderItem={({ item, index }) => {
          const { total } = item.tracks;
          const numTrack = `${total} track${total !== 1 && 's'}`;
          return (
            <PlaylistRow
              title={item.name}
              subTitle={numTrack}
              image={item.images[0]}
              // TODO - get the smallest image (sometimes a list has one, sometimes upto 3)
              action={e => this.open(e, item)}
            />
          );
        }
        }
      />
    );
  }
}
