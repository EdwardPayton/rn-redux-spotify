// @flow
import * as React from 'react';
import { TouchableOpacity } from 'react-native';
import MaterialIcon from './MaterialIcon';
import { default as s } from '../styles';

type Props = {
  icon: string,
  action: () => mixed
};

const ButtonIcon = (props: Props): React.Node => {
  const { icon, action } = props;
  return (
    <TouchableOpacity
      style={s.headerBack}
      onPress={action}
    >
      <MaterialIcon name={icon} />
    </TouchableOpacity>
  );
};

export default ButtonIcon;
