// @flow
import * as React from 'react';
import { View, Image as RnImage } from 'react-native';
import { default as s } from '../styles';

type Props = {
  uri: string,
  width?: number,
  height?: number,
  borderRadius?: number
}

const Image = (props: Props): React.Node => {
  const {
    uri, width, height, borderRadius,
  } = props;
  return (
    <View
      style={[s.listImageWrapper, s.shadow, { width, height, borderRadius }]}
    >
      <RnImage
        style={s.listImage}
        source={{ uri }}
      />
    </View>
  );
};

Image.defaultProps = {
  width: 60,
  height: 60,
  borderRadius: 3,
};

export default Image;
