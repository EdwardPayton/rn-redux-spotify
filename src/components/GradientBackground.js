// @flow
import * as React from 'react';
import RadialGradient from 'react-native-radial-gradient';
import { width, height } from '../styles/dimensions';
import { default as t } from '../styles/theme';

type Props = {
  children: React.Node,
  colorFrom?: string,
  colorTo?: string,
  withPadding?: boolean
}

const GradientBackground = (props: Props): React.Node => {
  const {
    children,
    colorFrom,
    colorTo,
    withPadding,
  } = props;
  return (
    <RadialGradient
      style={[{ width, height, paddingTop: withPadding ? t.barHeight : 0 }]}
      colors={[colorFrom, colorTo]}
      stops={[0.75]}
      center={[width, height]}
      radius={height}
    >
      {children}
    </RadialGradient>
  );
};

GradientBackground.defaultProps = {
  colorFrom: t.bgLight,
  colorTo: t.bgMain,
  withPadding: true,
};

export default GradientBackground;
