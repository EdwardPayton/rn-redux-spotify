// @flow
import * as React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { default as s } from '../styles';
import Image from './Image';

type Props = {
  title: string,
  subTitle?: string,
  image?: {
    url: string
  },
  action?: () => mixed
}

const PlaylistRow = (props: Props): React.Node => {
  const {
    title, subTitle, image, action,
  } = props;
  const rowTitle = (
    <Text numberOfLines={1} style={[s.textLarge, s.colorWhite]}>
      {title}
    </Text>
  );
  const rowSubtitle: React.Node = (
    <Text style={s.colorLight}>
      {subTitle}
    </Text>
  );
  return (
    <View style={s.playRowWrapper}>
      {image && <Image uri={image.url} />}
      {action
        ? (
          <TouchableOpacity
            onPress={action}
            style={s.playRow}
          >
            <View>
              {rowTitle}
              {subTitle && rowSubtitle}
            </View>
          </TouchableOpacity>
        )
        : (
          <View style={s.playRow}>
            {rowTitle}
            {subTitle && rowSubtitle}
          </View>
        )
      }
    </View>
  );
};

PlaylistRow.defaultProps = {
  subTitle: undefined,
  image: undefined,
  action: undefined,
};

export default PlaylistRow;
