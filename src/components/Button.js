// @flow
import * as React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import RadialGradient from 'react-native-radial-gradient';
import MaterialIcon from './MaterialIcon';
import { default as s } from '../styles';
import { default as t } from '../styles/theme';

type Props = {
  title: string,
  onPress: () => mixed,
  icon?: {
    name: string,
    size?: number
  },
  textColor?: string,
  bgColor?: string,
  borderFrom?: string,
  borderTo?: string,
  align?: string
}

const Button = (props: Props): React.Node => {
  const {
    title,
    onPress,
    icon,
    textColor,
    bgColor,
    borderFrom,
    borderTo,
    align = 'center', // TODO - not DRY, having to repeat here and in defaultProps to keep ESlint and Flow happy
  } = props;
  const layoutMap = {
    center: 'center',
    left: 'flex-start',
    right: 'flex-end',
  };
  return (
    <View style={[s.buttonWrapper, { justifyContent: layoutMap[align] }]}>
      <TouchableOpacity
        onPress={onPress}
      >
        <RadialGradient
          style={s.button}
          colors={[borderTo, borderFrom]}
          stops={[0.75]}
          center={[250, 100]}
          radius={250}
        >
          <View style={[s.buttonInner, { backgroundColor: bgColor }]}>
            <Text style={[s.textBold, s.textLarge, { color: textColor }, icon && s.buttonTextWIthIcon]}>
              {title}
            </Text>
            {icon
              && <MaterialIcon name={icon.name} size={icon.size} color={textColor} />
            }
          </View>
        </RadialGradient>
      </TouchableOpacity>
    </View>
  );
};

Button.defaultProps = {
  icon: undefined,
  textColor: t.fgPale,
  bgColor: t.bgMain,
  borderFrom: t.bgBright,
  borderTo: t.fgMain,
  align: 'center',
};

export default Button;
