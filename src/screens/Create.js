import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import { NavigationActions, withNavigationFocus } from 'react-navigation';
import { connect } from 'react-redux';
import { getTopArtists, getRecommendations } from '../actions';
import CreateRow from '../components/CreateRow';
import ListHeader from '../components/ListHeader';
import Button from '../components/Button';
import { default as s } from '../styles';
import { default as t } from '../styles/theme';
import GradientBackground from '../components/GradientBackground';

// TODO flow - this component uses react-navigation withNavigationFocus() HOC so needs appropriate types
class Create extends Component {
  constructor(props) {
    super(props);

    this.state = {
      wasFocused: false,
    };
  }

  handleSelectedArtist(artist) {
    this.props.getRecommendations(artist.id);

    this.navigateAction = NavigationActions.navigate({
      routeName: 'Recommended',
      params: { name: artist.name },
    });
  }

  onFocus() { // eslint-diable-line react/sort-comp
    if (this.props.api.topArtists === undefined || !this.props.api.topArtists.length) {
      this.props.getTopArtists();
    }

    this.setState({ wasFocused: true });
  }

  componentDidUpdate(prevProps) {
    // When receiving focus for the first time
    !this.state.wasFocused && this.props.isFocused && this.onFocus();

    // hold the app here until recommendations is ready
    const { recomendations, apiPending } = this.props.api;
    const prevRecomendations = prevProps.api.recomendations;

    if (recomendations && recomendations !== prevRecomendations) {
      this.props.screenProps.homeNav.dispatch(this.navigateAction);
    }
  }

  render() {
    const {
      api: { topArtists },
      player: { playActive },
    } = this.props;
    const offset = topArtists && topArtists.length;

    const moreButton = (
      <View style={s.createMoreButtonWrapper}>
        <Button
          title="More"
          onPress={() => this.props.getTopArtists(offset)}
          icon={{ name: 'expand-more' }}
        />
      </View>
    );

    // HELPER function
    const genreList = (genres) => {
      if (!genres.length) return;
      const firstThree = genres.slice(0, 3);
      return firstThree.map((genre) => {
        const strings = genre.split(' ');
        const capitalised = strings.map(str => str.substr(0, 1).toUpperCase() + str.substr(1));
        return capitalised.join(' ');
      }).join(', ');
    }; // TODO - it works, but it's not pretty. Fix this code

    return (
      <GradientBackground colorFrom={t.bgBrightPale} colorTo={t.bgBright}>
        {topArtists
          && (
            <FlatList
              style={{ marginBottom: playActive ? 80 : 20 }}
              data={topArtists}
              keyExtractor={(item, index) => index.toString()}
              ListHeaderComponent={(
                <View>
                  <ListHeader title="Create" fontSize={t.fontSizeTitle} />
                  <Text style={[s.textBold, s.textLarge, s.colorLight]}>
                    Make new playlists based on your favourite artists
                  </Text>
                </View>
              )}
              ListFooterComponent={moreButton}
              ListEmptyComponent={(
                <Text style={{ color: 'white' }}>
                  Getting your Top Artists
                </Text>
              )}
              renderItem={({ item }) => {
                const genres = genreList(item.genres);
                return (
                  // <PlaylistRow
                  <CreateRow
                    title={item.name}
                    subTitle={genres}
                    image={item.images[0]}
                    action={() => this.handleSelectedArtist(item)}
                  />
                );
              }
              }
            />
          )
        }
      </GradientBackground>
    );
  }
}

function mapStateToProps(state) {
  return {
    api: state.spotifyApi,
    player: state.spotifyPlayer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getTopArtists: (offset = 0) => dispatch(getTopArtists(offset)),
    getRecommendations: id => dispatch(getRecommendations(id)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(Create));
