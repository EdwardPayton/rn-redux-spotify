// @flow
import * as React from 'react';
import {
  View, ScrollView, Text, TouchableOpacity, Linking,
} from 'react-native';
import { connect } from 'react-redux';
import { logoutOfSpotify, spotifyGetUser } from '../actions';
import GradientBackground from '../components/GradientBackground';
import Button from '../components/Button';
import ListHeader from '../components/ListHeader';
import { default as s } from '../styles';
import { default as t } from '../styles/theme';

type Props = {
  logout: () => mixed,
  user: { userObj: { display_name: string } },
  player: { playActive: boolean }
};

class Info extends React.Component<Props> {
  static defaultProps = {
    playActive: false,
  }

  render(): React.Node {
    const {
      user: { userObj },
      player: { playActive },
    } = this.props;

    return (
      <GradientBackground colorFrom={t.bgPale} colorTo={t.bgLight}>
        <ScrollView style={{ marginBottom: playActive ? 90 : 20 }}>
          <ListHeader
            title="Info"
            fontSize={t.fontSizeTitle}
          />
          {userObj !== null
            && (
            <View>
              <Text style={[s.infoUserInfo, s.colorLight]}>
You are logged in to Spotify as
                <Text style={s.textBold}>
                  {userObj.display_name}
                </Text>
              </Text>
              <Button
                title="Logout"
                onPress={this.props.logout}
              />
            </View>
            )
          }
          <ListHeader
            title="What is this app?"
            fontSize={t.fontSizeTitle}
          />
          <Text style={[s.infoUserInfo, s.colorLight]}>
I made this app for two reasons. To explore the Spotify API, and spend more time using React Native. One of my favourite parts of the Spotify API is the ability to find new artists based on seeds of things I listen to, so this app is designed to make creating new playlists easy.
          </Text>
          <ListHeader
            title="Who is Edward Payton?"
            fontSize={t.fontSizeTitle}
          />
          <Text style={[s.infoUserInfo, s.colorLight]}>
I am a Front End Web Devloper from Bristol with expertise is JavaScript. I work full time, this is one of my side projects which I do to try out new technology.
          </Text>
          <ListHeader
            title="Is this app finished?"
            fontSize={t.fontSizeTitle}
          />
          <Text style={[s.infoUserInfo, s.colorLight]}>
There is a lot I plan to add, which will come in time. My list of improvements is long, the next feature will allow I will add is to increase the customisation options when creating a playlist. There are also bug fixes, cross platform fixes, more thorough testing, and optimisations stuff.
          </Text>
          <ListHeader
            title="How do I get in contact?"
            fontSize={t.fontSizeTitle}
          />
          <TouchableOpacity onPress={() => { Linking.openURL('mailto:edwardjpayton@gmail.com?subject="Hi Edward"'); }}>
            <Text style={[s.infoUserInfo, s.colorLight]}>
edwardjpayton@gmail.com
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </GradientBackground>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.spotifyAuth,
    user: state.spotifyUser,
    player: state.spotifyPlayer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(logoutOfSpotify()),
    getUser: () => dispatch(spotifyGetUser()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Info);
