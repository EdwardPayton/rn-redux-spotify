// @flow
import * as React from 'react';
import { View } from 'react-native';
import { StackNavigator, NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { spotifyGetUser } from '../actions';
import MainTabs from './MainTabs';
import PlaylistDetail from './PlaylistDetail';
import RecommendedTracks from './RecommendedTracks';
import PlayerBar from '../containers/PlayerBar';

type Props = {
  navigation: any, // TODO
  auth: {
    userLoggedIn: boolean
  },
  user: {
    getUserSuccess: boolean
  },
  player: {
    playActive: boolean
  },
  getUser: () => mixed
};

const HomeNav = StackNavigator({
  MainTabs: { screen: MainTabs },
  Detail: { screen: PlaylistDetail },
  Recommended: { screen: RecommendedTracks },
}, {
  headerMode: 'none',
  initialRouteName: 'MainTabs',
});

class Home extends React.Component<Props> {
  goToLoginScreen(): void { // eslint-disable-line react/sort-comp
    const navAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Login' }),
      ],
    });
    this.props.navigation.dispatch(navAction);
  }

  componentDidMount(): void {
    this.props.getUser();
  }

  componentWillReceiveProps(nextProps): void {
    // TODO replace with componentDidUpdate(prevProps)
    // TODO this wont work because no props are passed
    if (nextProps.auth.userLoggedIn === false) this.goToLoginScreen();
  }

  render(): React.Node {
    const {
      user: { getUserSuccess },
      player,
    } = this.props;
    return (
      <View style={{ flex: 1 }}>
        {getUserSuccess && <HomeNav />}
        {player.playActive && <PlayerBar />}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.spotifyAuth,
    user: state.spotifyUser,
    player: state.spotifyPlayer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getUser: () => dispatch(spotifyGetUser()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
