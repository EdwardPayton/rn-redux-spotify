import React, { Component } from 'react';
import { Text, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { withNavigationFocus } from 'react-navigation';
import { spotifyGetUserPlaylists, getPlaylistTracks } from '../actions';
import PlaylistRow from '../components/PlaylistRow';
import ListHeader from '../components/ListHeader';
import GradientBackground from '../components/GradientBackground';
import { default as t } from '../styles/theme';

// TODO flow - this component uses react-navigation withNavigationFocus() HOC so needs appropriate types
class Playlists extends Component {
  constructor(props) {
    super(props);

    // TODO - sort playlist by "| Made with my app"
    // assign it to local state
    // get playlist on mount, sort & then setstate on update
    // (update may trigger after setstate, so needs to be conditional)

    this.state = {
      wasFocused: false,
    };

    this.onRefresh = this.onRefresh.bind(this);
    this.onPress = this.onPress.bind(this);
  }

  onRefresh() { // eslint-diable-line react/sort-comp
    this.props.getUserPlaylists();
  }

  open(e, item) {
    this.props.getPlaylistTracks(item);
    this.props.screenProps.homeNav.navigate('Detail');
  }

  onPress() {
    this.props.navigation.navigate('Create');
  }

  organisePlaylist(disorgainisedList) {
    const madeBy = '| Made by my app';
    const matches = [];
    const remaining = [];
    disorgainisedList.forEach((item) => {
      item.name.includes(madeBy)
        ? matches.push(item)
        : remaining.push(item);
    });
    console.log('organisePlaylist', matches, remaining, [...matches, ...remaining]);
    return [...matches, ...remaining];
  }

  onFocus() {
    if (this.props.api.playlist === undefined || !this.props.api.playlists.length) {
      this.props.getUserPlaylists();
    }
    this.setState({ wasFocused: true });
  }

  componentDidUpdate(prevProps) {
    // When receiving focus for the first time
    !this.state.wasFocused && this.props.isFocused && this.onFocus();
    const {
      api: { playlists, apiPending },
    } = this.props;
    // https://stackoverflow.com/questions/48639336/check-if-two-arrays-contain-identical-objects-react-componentdidupdate
    // if(!apiPending && playlists && playlists !== prevProps.api.playlists) {
    //   const organised = this.organisePlaylist(playlists); // https://jsfiddle.net/0j4jfgc8/1/
    // }
  }

  render() {
    const {
      api: { playlists, apiPending },
      player: { playActive },
    } = this.props;

    return (
      <GradientBackground>
        {playlists
          && (
            <FlatList
              style={{ marginBottom: playActive ? 80 : 20 }}
              onRefresh={this.onRefresh}
              refreshing={apiPending}
              data={playlists}
              keyExtractor={(item, index) => index.toString()}
              ListHeaderComponent={
                <ListHeader title="Playlists" fontSize={t.fontSizeTitle} />
              }
              ListEmptyComponent={(
                <Text style={{ color: 'white' }}>
                  Getting your playlists
                </Text>
              )}
              renderItem={({ item, index }) => {
                const { total } = item.tracks;
                const numTrack = `${total} track${total !== 1 && 's'}`;
                return (
                  <PlaylistRow
                    title={item.name}
                    subTitle={numTrack}
                    image={item.images[0]}
                    // TODO - get the smallest image (sometimes a list has one, sometimes upto 3)
                    action={e => this.open(e, item)}
                  />
                );
              }
              }
            />
          )
        }
        {/* FAB button - playlist-add icon */}
      </GradientBackground>
    );
  }
}

function mapStateToProps(state) {
  return {
    api: state.spotifyApi,
    player: state.spotifyPlayer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getUserPlaylists: () => dispatch(spotifyGetUserPlaylists()),
    getPlaylistTracks: item => dispatch(getPlaylistTracks(item)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(Playlists));
