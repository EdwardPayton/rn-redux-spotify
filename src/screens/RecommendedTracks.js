// @flow
import * as React from 'react';
import {
  View, FlatList, Button, TextInput,
} from 'react-native';
import { connect } from 'react-redux';
import { createPlaylist, addTrackToPlaylist } from '../actions';
import ListHeader from '../components/ListHeader';
import PlaylistRow from '../components/PlaylistRow';
import GradientBackground from '../components/GradientBackground';

type Props = {
  navigation: any, // TODO
  api: {
    recomendations: {
      tracks: any[]
    },
    newPlaylist: {},
    tracksAdded: boolean
  },
  player: {
    playActive: boolean
  },
  createPlaylist: (name: string) => mixed,
  addTrackToPlaylist: () => mixed
};

type State = {
  showInput: boolean,
  artistName: string,
  playlistName: string
}

class RecommendedTracks extends React.Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      showInput: false,
      artistName: '',
      playlistName: '',
    };

    (this: any).namePlaylist = this.namePlaylist.bind(this);
    (this: any).createPlaylist = this.createPlaylist.bind(this);
  }

  namePlaylist(): void { // eslint-diable-line react/sort-comp
    this.setState({ showInput: true });
  }

  createPlaylist(): void {
    this.props.createPlaylist(this.state.playlistName);
  }

  returnToPlaylists(): void {
    this.props.navigation.goBack();
  }

  componentDidUpdate(prevProps): void {
    const {
      api: { newPlaylist, tracksAdded },
    } = this.props;
    if (newPlaylist && !prevProps.api.newPlaylist) this.props.addTrackToPlaylist();
    if (tracksAdded && !prevProps.api.tracksAdded) this.returnToPlaylists();
  }

  componentWillMount(): void {
    const { params } = this.props.navigation.state;

    this.setState({
      artistName: params.name,
      playlistName: `Based on ${params.name}`,
    });
  }

  render(): React.Node {
    const {
      api: { recomendations },
      player: { playActive },
    } = this.props;

    const handleBack = () => this.props.navigation.goBack();

    const handleNamePlaylist = <Button onPress={this.namePlaylist} title="Name Playlist" />;

    const enterPlaylistName = (
      <View>
        <TextInput
          autoFocus
          style={{ height: 50, borderColor: 'gray', borderWidth: 1 }}
          onChangeText={playlistName => this.setState({ playlistName })}
          value={this.state.playlistName}
        />
        <Button
          disabled={this.state.playlistName.length < 2}
          onPress={this.createPlaylist}
          title="OK"
        />
      </View>
    );

    return (
      <GradientBackground withPadding={false}>
        <View>
          <ListHeader
            title={`Tracks related to ${this.state.artistName}`}
            backAction={handleBack}
          />
          {handleNamePlaylist}
          {this.state.showInput && enterPlaylistName}
        </View>
        {recomendations
          && (
            <FlatList
              // style={styles.list}
              style={{ marginBottom: playActive ? 80 : 20 }}
              data={recomendations.tracks}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => (
                <PlaylistRow
                  title={`${item.name} - ${item.artists[0].name}`}
                />
              )}
            />
          )
        }
      </GradientBackground>
    );
  }
}

function mapStateToProps(state) {
  return {
    api: state.spotifyApi,
    player: state.spotifyPlayer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    createPlaylist: name => dispatch(createPlaylist(name)),
    addTrackToPlaylist: () => dispatch(addTrackToPlaylist()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RecommendedTracks);
