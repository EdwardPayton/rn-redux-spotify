// @flow
import * as React from 'react';
import { View } from 'react-native';
import { TabNavigator } from 'react-navigation';
import Playlists from './Playlists';
import Info from './Info';
import Create from './Create';
import TabBar from '../containers/TabBar';

type Props = {
  navigation: any // TODO
};

const TabNav = TabNavigator({
  Playlists: { screen: Playlists },
  Create: { screen: Create },
  Info: { screen: Info },
}, {
  initialRouteName: 'Create',
  tabBarComponent: TabBar,
  lazy: false,
});

export default class MainTabs extends React.Component<Props> {
  static navigationOptions = {
    header: null,
  };

  render(): React.Node {
    return (
      <View style={{ flex: 1, position: 'relative' }}>
        <TabNav screenProps={{ homeNav: this.props.navigation }} />
      </View>
    );
  }
}
