// @flow
import * as React from 'react';
import { Animated, View, Image } from 'react-native';
import { connect } from 'react-redux';
import { startPlayback } from '../actions';
import Button from '../components/Button';
import ButtonIcon from '../components/ButtonIcon';
import PlaylistDetailContent from '../containers/PlaylistDetailContent';
import GradientBackground from '../components/GradientBackground';
import { default as s } from '../styles';
import { default as t } from '../styles/theme';

type Props = {
  navigation: any, // TODO
  api: {
    playlistTracks: {
      images: any[],
      name: string
    },
    apiPending: boolean
  },
  startPlayback: (playlist: {}, index: ?number, seconds: ?number) => mixed,

};

type State = {
  scrollY: number
};

const SCROLL_HEIGHT = t.scrollHeaderMaxHeight - t.barHeight;
class PlaylistDetail extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      scrollY: new Animated.Value(0),
    };

    (this: any).backToPlaylists = this.backToPlaylists.bind(this);
    (this: any).playFromStart = this.playFromStart.bind(this);
  }

  backToPlaylists(): void {
    this.props.navigation.goBack();
  }

  playFromStart(): void {
    const {
      api: { playlistTracks },
    } = this.props;
    this.props.startPlayback(playlistTracks, 0, 0);
  }

  render(): React.Node {
    const {
      api: { playlistTracks, apiPending },
    } = this.props;

    const scrollY = Animated.add(this.state.scrollY, 0);
    const wrapTranslate = scrollY.interpolate({
      inputRange: [0, SCROLL_HEIGHT],
      outputRange: [0, -SCROLL_HEIGHT],
      extrapolate: 'clamp',
    });
    const imageOpacity = scrollY.interpolate({
      inputRange: [0, SCROLL_HEIGHT / 2, SCROLL_HEIGHT],
      outputRange: [1, 1, 0],
      extrapolate: 'clamp',
    });
    const imageTranslate = scrollY.interpolate({
      inputRange: [0, SCROLL_HEIGHT],
      outputRange: [0, 100],
      extrapolate: 'clamp',
    });
    const titleOpacity = scrollY.interpolate({
      inputRange: [0, SCROLL_HEIGHT - t.barHeight, SCROLL_HEIGHT],
      outputRange: [0, 0, 1],
      extrapolate: 'clamp',
    });
    const buttonTranslate = scrollY.interpolate({
      inputRange: [0, SCROLL_HEIGHT],
      outputRange: [0, -70],
      extrapolate: 'clamp',
    });

    return (
      <GradientBackground withPadding={false}>
        {playlistTracks && !apiPending && (
          <View style={s.flex1}>

            {/* Image wrapper */}
            <Animated.View style={[s.scrollImageWrap,
              { transform: [{ translateY: wrapTranslate }] }]}
            >
              <Animated.View style={[
                s.scrollImageParallax,
                {
                  opacity: imageOpacity,
                  transform: [{ translateY: imageTranslate }],
                }]}
              >
                <Image
                  style={s.scrollImage}
                  source={{ uri: playlistTracks.images[0].url }}
                />
                <Animated.Text style={s.scrollPlaylistName} numberOfLines={2}>
                  {playlistTracks.name}
                </Animated.Text>
                <Animated.View style={[s.scrollPlaylistBtn,
                  { transform: [{ translateY: buttonTranslate }] }]}
                >
                  <Button
                    title="Play from start"
                    onPress={this.playFromStart}
                    icon={{ name: 'play-arrow' }}
                  />
                </Animated.View>
              </Animated.View>
            </Animated.View>

            {/* Header bar */}
            <View style={s.scrollHeaderBar}>
              <Animated.View style={[s.scrollHeaderBg, { opacity: titleOpacity }]} />
              <ButtonIcon icon="arrow-back" action={this.backToPlaylists} />
              <Animated.Text
                style={[
                  s.headerTitle,
                  s.scrollHeaderTitle,
                  { opacity: titleOpacity }]}
                numberOfLines={1}
              >
                {playlistTracks.name}
              </Animated.Text>
            </View>

            {/* Scroll view */}
            <Animated.ScrollView
              style={s.flex1}
              scrollEventThrottle={1}
              onScroll={Animated.event(
                [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
                { useNativeDriver: true },
              )}
            >
              <View style={s.scrollViewContent}>
                <PlaylistDetailContent />
              </View>
            </Animated.ScrollView>
          </View>
        )}
      </GradientBackground>
    );
  }
}
function mapStateToProps(state) {
  return {
    api: state.spotifyApi,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    startPlayback: (playlist, index, seconds) => dispatch(startPlayback(playlist, index, seconds)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PlaylistDetail);
