// @flow
import * as React from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { initialiseSpotify, getSpotifyLoginStatus, loginToSpotify } from '../actions';

type Props = {
  navigation: any, // TODO
  init: {
    initSuccess: boolean
  },
  auth: {
    userLoggedIn: boolean
  },
  initialise: () => mixed,
  loginStatus: () => mixed,
  login: () => mixed
}
class Login extends React.Component<Props> {
  goToPlayer(): void { // eslint-disable-line react/sort-comp
    const navAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Home' }),
      ],
    });
    this.props.navigation.dispatch(navAction);
  }

  componentDidMount(): void {
    this.props.initialise();
  }

  componentWillReceiveProps(nextProps): void {
    // TODO replace with componentDidUpdate(prevProps)

    if (this.props.init.initSuccess === false && nextProps.init.initSuccess) {
      this.props.loginStatus();
    }
    if (nextProps.auth.userLoggedIn && nextProps.init.initSuccess) this.goToPlayer();
  }

  render(): React.Node {
    const {
      init: { initSuccess },
    } = this.props;

    return (
      initSuccess
        ? (
          <View style={styles.container}>
            <Text style={styles.greeting}>
              Hey! You! Log into your spotify
            </Text>
            <TouchableHighlight onPress={this.props.login} style={styles.spotifyLoginButton}>
              <Text style={styles.spotifyLoginButtonText}>
                Log into Spotify
              </Text>
            </TouchableHighlight>
          </View>
        ) : (
          <View style={styles.container}>
            <ActivityIndicator animating style={styles.loadIndicator} />
            <Text style={styles.loadMessage}>
              Loading...
            </Text>
          </View>
        )
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },

  loadIndicator: {
    //
  },
  loadMessage: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },

  spotifyLoginButton: {
    justifyContent: 'center',
    borderRadius: 18,
    backgroundColor: 'green',
    overflow: 'hidden',
    width: 200,
    height: 40,
    margin: 20,
  },
  spotifyLoginButtonText: {
    fontSize: 20,
    textAlign: 'center',
    color: 'white',
  },

  greeting: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

const mapStateToProps = state => ({
  init: state.spotifyInit,
  auth: state.spotifyAuth,
});

const mapDispatchToProps = dispatch => ({
  initialise: () => dispatch(initialiseSpotify()),
  loginStatus: () => dispatch(getSpotifyLoginStatus()),
  login: () => dispatch(loginToSpotify()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
