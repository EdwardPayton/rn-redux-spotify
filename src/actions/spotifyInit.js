import Spotify from 'rn-spotify-sdk';
import { INIT_PENDING, INIT_SUCCESS, INIT_FAILURE } from '../constants';

const SPOTIFY_OPTIONS = {
  clientID: 'e5643be315bc4c1ead72ef03b3f5d128',
  sessionUserDefaultsKey: 'SpotifySession',
  redirectURL: 'examplespotifyapp://auth',
  scopes: [
    'user-read-private',
    'user-top-read',
    'playlist-read',
    'playlist-read-private',
    'playlist-modify-public',
    'playlist-modify-private',
    'streaming',
  ],
  tokenSwapURL: 'https://rn-redux-spotify.herokuapp.com/swap',
  tokenRefreshURL: 'https://rn-redux-spotify.herokuapp.com/refresh',
};

export function initialiseSpotify() { // eslint-disable-line import/prefer-default-export
  return (dispatch) => {
    dispatch(isPending());
    Spotify.isInitializedAsync()
      .then((initalised) => {
        initalised
          ? dispatch(isInitialised())
          : Spotify.initialize(SPOTIFY_OPTIONS)
            .then(() => dispatch(isInitialised()))
            .catch(() => dispatch(hasError()));
      });
  };
}

function isPending() {
  return {
    type: INIT_PENDING,
  };
}

function isInitialised() {
  return {
    type: INIT_SUCCESS,
  };
}

function hasError() {
  return {
    type: INIT_FAILURE,
  };
}
