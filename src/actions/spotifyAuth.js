import Spotify from 'rn-spotify-sdk';
import {
  STATUS_PENDING, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT_SUCCESS,
} from '../constants';

export function loginToSpotify() {
  return (dispatch, getState) => {
    const currentStatus = getState().spotifyAuth.userLoggedIn;
    dispatch(statusPending(currentStatus));
    Spotify.login()
      .then(loggedIn => (loggedIn
        ? dispatch(loginSuccess())
        : dispatch(loginFailure())));
  };
}

export function logoutOfSpotify() {
  return (dispatch, getState) => {
    const currentStatus = getState().spotifyAuth.userLoggedIn;
    dispatch(statusPending(currentStatus));
    Spotify.logout()
      .finally(() => dispatch(logoutSuccess()));
  };
}

export function getSpotifyLoginStatus() {
  return (dispatch) => {
    Spotify.isLoggedInAsync()
      .then((result) => {
        result
          ? dispatch(loginSuccess())
          : dispatch(loginFailure());
      });
  };
}

function statusPending(currentStatus) {
  return {
    type: STATUS_PENDING,
    currentStatus,
  };
}

function loginSuccess() {
  return {
    type: LOGIN_SUCCESS,
  };
}

function loginFailure() {
  return {
    type: LOGIN_FAILURE,
  };
}

function logoutSuccess() {
  return {
    type: LOGOUT_SUCCESS,
  };
}
