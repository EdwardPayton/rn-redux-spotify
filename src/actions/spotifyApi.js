import {
  ACESSS_API_PENDING,
  GET_USER_PLAYLISTS_SUCCESS,
  // GET_FEATURED_PLAYLISTS_SUCCESS,
  GET_PLAYLISTS_FAILURE,
  PLAYLIST_TRACKS_SUCCESS,
  PLAYLIST_TRACK_FAILURE,
  GET_TOP_ARTISTS_SUCCESS,
  GET_TOP_ARTISTS_FAILURE,
  GET_RECOMENDATIONS_SUCCESS,
  GET_RECOMENDATIONS_FAILURE,
  CREATE_PLAYLIST_SUCCESS,
  CREATE_PLAYLIST_FAILURE,
  TRACKS_ADDED_SUCCESS,
  TRACKS_ADDED_FAILURE,
} from '../constants';

const buildUrl = (userId, listId = '') => `https://api.spotify.com/v1/users/${userId}/playlists/${listId}`;

export function spotifyGetUserPlaylists() {
  return (dispatch, getState) => {
    const {
      spotifyToken: { tokenObj },
      spotifyUser: { userObj },
    } = getState();
    const token = tokenObj.accessToken;
    const userId = userObj.id;
    const url = buildUrl(userId);
    const config = {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    };
    dispatch(accessApiPending());
    fetch(url, config)
      .then(result => (result.ok
        ? result.json() // convert the good response into json
        : Promise.reject(result))) // trigger the catch()
      .then(json => dispatch(getUserPlaylistsSuccess(json.items)))
      .catch(error => dispatch(getPlaylistsFailure(error)));
  };
}

export function getPlaylistTracks(data) {
  return (dispatch, getState) => {
    const {
      spotifyToken: { tokenObj },
      spotifyUser: { userObj },
    } = getState();
    const token = tokenObj.accessToken;
    const userId = userObj.id;
    const url = `https://api.spotify.com/v1/users/${userId}/playlists/${data.id}`;
    const config = {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    };
    dispatch(accessApiPending());
    fetch(url, config)
      .then(result => (result.ok
        ? result.json()
        : Promise.reject(result)))
      .then(json => dispatch(playlistTracksSuccess(json)))
      .catch(() => dispatch(playlistTracksFailure()));
  };
}

export function getTopArtists(offset = 0, limit = 5) {
  return (dispatch, getState) => {
    const {
      spotifyToken: { tokenObj },
      spotifyApi: { topArtists },
    } = getState();
    const token = tokenObj.accessToken;
    const url = `https://api.spotify.com/v1/me/top/artists?limit=${limit}&offset=${offset}&time_range=medium_term`;
    const config = {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    };
    dispatch(accessApiPending());
    fetch(url, config)
      .then(result => (result.ok
        ? result.json()
        : Promise.reject(result)))
      .then((json) => {
        const newItems = topArtists ? topArtists.concat(json.items) : json.items;
        return dispatch(getTopArtistsSuccess(newItems));
      })
      .catch(error => dispatch(getTopArtistsFailure(error)));
  };
}

export function getRecommendations(artistId) {
  return (dispatch, getState) => {
    const {
      spotifyToken: { tokenObj },
    } = getState();
    const token = tokenObj.accessToken;
    const url = `https://api.spotify.com/v1/recommendations?seed_artists=${artistId}&limit=50`;
    const config = {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    };
    fetch(url, config)
      .then(result => (!result.error
        ? result.json()
        : Promise.reject(result)))
      .then(json => dispatch(getRecommendationsSuccess(json)))
      .catch(error => dispatch(getRecommendationsFailure(error)));
  };
}

export function createPlaylist(name) {
  return (dispatch, getState) => {
    const {
      spotifyToken: { tokenObj },
      spotifyUser: { userObj },
    } = getState();
    const token = tokenObj.accessToken;
    const url = `https://api.spotify.com/v1/users/${userObj.id}/playlists`;
    const body = {
      name: `${name} | Made with my app`,
      description: 'Created by EP',
    };
    const config = {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    };
    dispatch(accessApiPending());
    fetch(url, config)
      .then(result => (result.ok
        ? result.json() // convert the good response into json
        : Promise.reject(result))) // trigger the catch()
      .then(json => dispatch(createPlaylistSuccess(json)))
      .catch(error => dispatch(createPlaylistFailure(error)));
  };
}

export function addTrackToPlaylist() {
  return (dispatch, getState) => {
    const {
      spotifyToken: { tokenObj },
      spotifyUser: { userObj },
      spotifyApi: { recomendations, newPlaylist },
    } = getState();
    const token = tokenObj.accessToken;
    const URIs = recomendations.tracks.map(track => track.uri);
    const encodedURIs = encodeURIComponent(URIs.join(','));
    const { id, name } = newPlaylist;
    const url = `https://api.spotify.com/v1/users/${userObj.id}/playlists/${id}/tracks?position=0&uris=${encodedURIs}`;
    const body = {
      name,
      description: 'Created by EP',
    };
    const config = {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    };
    dispatch(accessApiPending());
    fetch(url, config)
      .then(result => (result.ok
        ? result.json() // convert the good response into json
        : Promise.reject(result))) // trigger the catch()
      .then(json => dispatch(tracksAddedSuccess(json)))
      .catch(error => dispatch(tracksAddedFailure(error)));
  };
}

function accessApiPending() {
  return {
    type: ACESSS_API_PENDING,
  };
}

function getUserPlaylistsSuccess(data) {
  return {
    type: GET_USER_PLAYLISTS_SUCCESS,
    data,
  };
}

function playlistTracksSuccess(data) {
  return {
    type: PLAYLIST_TRACKS_SUCCESS,
    data,
  };
}

function playlistTracksFailure() {
  return {
    type: PLAYLIST_TRACK_FAILURE,
  };
}

function getTopArtistsSuccess(data) {
  return {
    type: GET_TOP_ARTISTS_SUCCESS,
    data,
  };
}

function getTopArtistsFailure() {
  return {
    type: GET_TOP_ARTISTS_FAILURE,
  };
}

function getPlaylistsFailure() {
  return {
    type: GET_PLAYLISTS_FAILURE,
  };
}

function getRecommendationsSuccess(data) {
  return {
    type: GET_RECOMENDATIONS_SUCCESS,
    data,
  };
}

function getRecommendationsFailure() {
  return {
    type: GET_RECOMENDATIONS_FAILURE,
  };
}

function createPlaylistSuccess(data) {
  return {
    type: CREATE_PLAYLIST_SUCCESS,
    data,
  };
}

function createPlaylistFailure() {
  return {
    type: CREATE_PLAYLIST_FAILURE,
  };
}

function tracksAddedSuccess(data) {
  return {
    type: TRACKS_ADDED_SUCCESS,
    data,
  };
}

function tracksAddedFailure() {
  return {
    type: TRACKS_ADDED_FAILURE,
  };
}
