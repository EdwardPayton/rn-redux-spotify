import Spotify from 'rn-spotify-sdk';
import { GET_USER_PENDING, GET_USER_SUCCESS, GET_USER_FAILURE } from '../constants';

export function spotifyGetUser() { // eslint-disable-line import/prefer-default-export
  return (dispatch) => {
    dispatch(getUserPending());
    Spotify.getMe()
      .then(data => dispatch(getUserSuccess(data)))
      .catch(error => dispatch(getUserFailure(error)));
  };
}

function getUserPending() {
  return {
    type: GET_USER_PENDING,
  };
}

function getUserSuccess(data) {
  return {
    type: GET_USER_SUCCESS,
    data,
  };
}

function getUserFailure(data) {
  return {
    type: GET_USER_FAILURE,
    data,
  };
}
