import Spotify from 'rn-spotify-sdk';
import {
  PLAY_PENDING,
  PLAY_SUCCESS,
  PLAY_PAUSE,
  PLAY_RESUME,
  PLAY_FAILURE,
  TRACK_CHANGE_SUCCESS,
  TRACK_CHANGE_FAILURE,
  SET_CURRENT_TRACK,
} from '../constants';

export function startPlayback(playlist, index = 0, seconds = 0) {
  return (dispatch) => {
    dispatch(playPending());
    Spotify.playURI(playlist.uri, index, seconds)
      .then(
        () => dispatch(playSuccess(playlist)),
        error => dispatch(playFailure(error)),
      );
  };
}

/**
 *
 * @param {boolean} state - play or pause the track. true = play, false = pause
 */
export function setPlayingState(state = false) {
  return (dispatch, getState) => {
    const meta = getState().spotifyPlayer.track;
    Spotify.setPlaying(state)
      .then(() => {
        if (state) dispatch(playResume(meta));
        else dispatch(playPause(meta));
      });
  };
}

export function playNext() {
  return (dispatch) => {
    dispatch(playPending());
    Spotify.skipToNext()
      .then(
        () => dispatch(trackChangeSuccess()),
        () => dispatch(trackChangeFailure()),
      );
  };
}

export function playPrev() {
  return (dispatch) => {
    dispatch(playPending());
    Spotify.skipToPrevious()
      .then(
        () => dispatch(trackChangeSuccess()),
        () => dispatch(trackChangeFailure()),
      );
  };
}

export function setCurrentTrack(track) {
  return {
    type: SET_CURRENT_TRACK,
    data: track,
  };
}

//

function playPending() {
  return {
    type: PLAY_PENDING,
  };
}

function playSuccess(data) {
  return {
    type: PLAY_SUCCESS,
    data,
  };
}

function playPause(data) {
  return {
    type: PLAY_PAUSE,
    data,
  };
}

function playResume(data) {
  return {
    type: PLAY_RESUME,
    data,
  };
}

function playFailure(data) {
  return {
    type: PLAY_FAILURE,
    data,
  };
}

function trackChangeSuccess() {
  return {
    type: TRACK_CHANGE_SUCCESS,
  };
}

function trackChangeFailure() {
  return {
    type: TRACK_CHANGE_FAILURE,
  };
}
