import { StyleSheet } from 'react-native';
import { default as t } from './theme';
import { width } from './dimensions';

const styles = StyleSheet.create({
  // Flexbox
  flex1: {
    flex: 1,
  },

  // Text
  text: {
    textAlign: 'center',
  },
  textBold: {
    fontWeight: 'bold',
  },
  textTitle: {
    fontSize: t.fontSizeTitle,
  },
  textLarge: {
    fontSize: t.fontSizeL,
  },
  textCenter: {
    textAlign: 'center',
  },

  // Colors
  colorWhite: {
    color: '#fff',
  },
  colorLight: {
    color: t.fgPale,
  },
  colorBgBright: {
    color: t.bgBright,
  },
  colorBgPale: {
    color: t.bgPale,
  },
  colorText: {
    color: t.bgText,
  },

  // Shadow
  shadow: {
    backgroundColor: t.bgMain,
    shadowColor: t.bgMain,
    shadowOpacity: 1,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 1,
    },
    elevation: 5,
  },

  // Button
  buttonWrapper: {
    flexDirection: 'row',
    // justifyContent: 'center',
  },
  button: {
    overflow: 'hidden',
    padding: 1,
    borderRadius: 50,
  },
  buttonInner: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 3,
    height: 50,
    paddingLeft: 25,
    paddingRight: 25,
    // backgroundColor: t.bgMain,
    borderRadius: 45,
  },
  buttonTextWIthIcon: {
    paddingLeft: 10,
  },

  // ListImage
  listImageWrapper: {
    overflow: 'hidden',
  },
  listImage: {
    width: '100%',
    height: '100%',
  },

  // TabBar
  tabBar: {
    // backgroundColor: t.bgMain,
    flexDirection: 'row',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: t.barHeight,
    zIndex: 2,
  },
  tabTab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: t.barHeight,
  },
  tabText: {
    textAlign: 'center',
    height: 22,
  },
  tabIconWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 30,
  },

  // List Header
  headerBar: {
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'center',
    height: t.barHeight,
  },
  headerBack: {
    width: 40,
    justifyContent: 'center',
  },
  headerTitle: {
    position: 'absolute',
    left: 50,
    right: 45, // 45 not 50 so slightly longer titles are not truncated. Visually works better
    fontWeight: 'bold',
    textAlign: 'center',
    color: t.fgPale,
  },

  // List item
  // listItem: {
  //   flex: 1,
  //   height: 50,
  //   flexDirection: 'row',
  //   alignItems: 'center',
  //   paddingLeft: 5,
  //   paddingRight: 5,
  //   borderBottomWidth: 2
  // },

  // Loader
  loadWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF', // TODO
  },
  loadIndicator: {
    //
  },
  loadMessage: {
    fontSize: t.fontSizeTitle,
    textAlign: 'center',
    margin: 10,
  },

  // Platlist Row
  playRowWrapper: {
    flex: 1,
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
  },
  playRow: {
    flex: 1,
    height: 80,
    paddingLeft: 10,
    paddingRight: 5,
  },

  // Create
  createMoreButtonWrapper: {
    paddingTop: 10,
    paddingBottom: 30,
  },

  // Create row
  createRowWrapper: {
    position: 'relative',
    flex: 1,
    flexDirection: 'row',
    // alignItems: 'center',
    height: 200,
    padding: 10,
    marginBottom: 10,
  },
  createRowColumn: {
    width: '50%',
  },
  createRowColumnR: {
    paddingTop: 10,
  },
  createButtonWrapper: {
    position: 'absolute',
    bottom: 10,
    left: -2, // visually aligns the button
  },

  // User Info
  infoUserInfo: {
    fontSize: t.fontSizeL,
    textAlign: 'center',
    margin: 10,
  },
  infoUserName: {
    fontWeight: 'bold',
  },

  // Playlist Detail
  scrollImageWrap: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: t.scrollHeaderMaxHeight,
    overflow: 'hidden',
    zIndex: 1,
  },
  scrollImageParallax: {
    height: t.scrollHeaderMaxHeight,
    overflow: 'hidden',
  },
  scrollImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: t.scrollHeaderMaxHeight,
    opacity: 0.2,
    resizeMode: 'cover',
  },
  scrollPlaylistName: {
    position: 'absolute',
    top: 100,
    left: 50,
    right: 50,
    textAlign: 'center',
    fontSize: t.fontSizeTitle,
    fontWeight: 'bold',
    color: t.fgPale,
  },
  scrollPlaylistBtn: {
    position: 'absolute',
    top: 200,
    width: '100%',
    paddingTop: 20,
    paddingBottom: 35,
  },
  scrollHeaderBar: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: t.barHeight,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    backgroundColor: 'transparent',
    zIndex: 2,
  },
  scrollHeaderTitle: {
    fontSize: t.fontSizeL,
  },
  scrollHeaderBg: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: t.barHeight,
    backgroundColor: t.bgMain,
    // zIndex: 2
  },
  scrollViewContent: {
    paddingTop: t.scrollHeaderMaxHeight + 20,
  },

  // Player Bar
  playBar: {
    flexDirection: 'row',
    // height: t.barHeight,
    backgroundColor: t.bgMain,
  },
  playBarEdge: {
    width: t.barHeight,
    height: t.barHeight,
  },
  playBarCenter: {
    width: width - (t.barHeight * 2),
    height: t.barHeight,
    padding: 5,
  },
  controlPlay: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
