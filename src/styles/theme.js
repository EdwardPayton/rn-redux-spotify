
export default {
  // Colors
  bgMain: '#141336',
  bgLight: '#38375E',
  bgPale: '#6e6ba9',
  bgDark: '#02025c',
  bgBright: '#5360c2',
  bgBrightPale: '#9092D6',
  bgText: '#C4C8FF',

  fgMain: '#ffa734',
  fgLight: '#ffc881',
  fgPale: '#FFDEB3',
  fgDark: '#935301',
  fgBright: '#D97F09',

  // Dimensions
  barHeight: 70,
  scrollHeaderMaxHeight: 300,

  // Fonts
  fontSizeTitle: 25,
  fontSizeL: 18,
  fontSizeM: 14,
  fontSizeS: 12,

  // Icons
  iconSizeL: 50,
  iconSizeM: 40,

  // Padding
  paddingL: 35,
  paddingM: 20,
  paddingS: 10,
  paddingXS: 5,
};
