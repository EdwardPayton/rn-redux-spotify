import Spotify from 'rn-spotify-sdk';
import { RENEW_TOKEN_PENDING, RENEW_TOKEN_SUCCESS, RENEW_TOKEN_FAILURE } from '../constants';

/*
  TODO
  - NOT WORKING
  -  check the action type before running getNewToken
    (should not run if spotify is not initialised, if user is logging out)
  - expiresTime does not work
*/

/**
 * Auth Middleware
 * Checks the spotify access_token expiry and refreshes it if needed
 * Based on: http://reactnative.vn/create-redux-middle-ware-to-refresh-token-in-react-native-app/
 */
export default function authMiddleware({ dispatch, getState }) {
  return next => (action) => {
    if (typeof action === 'function') {
      const state = getState();
      // console.log('MIDDLEWARE tokenObj:', state, state.spotifyToken.tokenObj)
      // if token object exists and is about to expire
      if (state.spotifyToken.tokenObj && isExpired(state.spotifyToken.expires)) {
        // check if a request is already pending
        console.log(1, state);
        if (!state.spotifyToken.tokenPending) {
          return getNewToken(dispatch)
            .then(() => next(action));
        }
      }
      // else if no token object
      else if (state.spotifyToken.tokenObj === null) {
        return getNewToken(dispatch)
          .then(() => next(action));
      }
    }
    return next(action);
  };
}

function isExpired(expires) {
  const currentTime = new Date();
  const expireTime = new Date(expires);
  return true;
  // return currentTime >= expireTime; // TODO doesnt work
}

function getNewToken(dispatch) {
  const getNewTokenPromise = Spotify.getAuthAsync()
    .then((data) => {
      console.warn('TOKEN', data);
      const date = new Date();
      const expires = date.setHours(date.getHours() + 1);
      dispatch({
        type: RENEW_TOKEN_SUCCESS,
        data,
        expires,
      });
      return data
        ? Promise.resolve(data)
        : Promise.reject({ message: 'could not refresh token' });
    })
    .catch((error) => {
      dispatch({ type: RENEW_TOKEN_FAILURE });
    });

  dispatch({ type: RENEW_TOKEN_PENDING });

  return getNewTokenPromise;
}
